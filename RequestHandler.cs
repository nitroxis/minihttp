﻿using System.Threading.Tasks;

namespace Http
{
	/// <summary>
	/// Asynchronous request handler delegate.
	/// </summary>
	/// <param name="context">The <see cref="HttpContext"/> that holds request and response.</param>
	/// <returns>A tasks whose result may be true if the request has been handled or false otherwise.</returns>
	public delegate Task<bool> RequestHandler(HttpContext context);
}
