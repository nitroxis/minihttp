﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Http
{
	/// <summary>
	/// Represents a simple HTTP server.
	/// </summary>
	public class HttpServer
	{
		#region Fields

		private readonly List<IHost> hosts;
		private bool running;
		private readonly object handlerLock;
		private List<RequestHandler> handlers;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets a list of request handlers.
		/// </summary>
		public List<RequestHandler> Handlers
		{
			get
			{
				lock (this.handlerLock)
					return this.handlers;
			}
			set
			{
				lock (this.handlerLock)
					this.handlers = value;
			}
		}

		/// <summary>
		/// Gets or sets the response that is sent when no suitable handler was found.
		/// </summary>
		public HttpStatusHandlerCollection ErrorHandlers { get; set; }

		/// <summary>
		/// Gets or sets the name of the application as shows in default responses.
		/// </summary>
		public string ApplicationName { get; set; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new HttpServer.
		/// </summary>
		public HttpServer(params IHost[] hosts)
		{
			// try to get app name from assembly attributes, in case this code is embedded somewhere.
			Assembly assembly = typeof(HttpServer).Assembly;
			AssemblyTitleAttribute[] titleAttributes = (AssemblyTitleAttribute[])assembly.GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
			AssemblyVersionAttribute[] versionAttributes = (AssemblyVersionAttribute[])assembly.GetCustomAttributes(typeof(AssemblyVersionAttribute), false);

			string title = "mini-http";
			string version = "1.0";

			if (titleAttributes.Length > 0)
				title = titleAttributes[0].Title;
			if (versionAttributes.Length > 0)
				version = versionAttributes[0].Version;

			this.ApplicationName = $"{title}/{version}";
			this.hosts = new List<IHost>();

			foreach (IHost host in hosts)
				this.AddHost(host);

			this.handlerLock = new object();
			this.handlers = new List<RequestHandler>();
			this.ErrorHandlers = new HttpStatusHandlerCollection();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Starts the server and all hosts.
		/// </summary>
		public void Start()
		{
			lock (this)
			{
				if (this.running)
					throw new Exception("Server is already running.");

				this.running = true;

				foreach (IHost host in this.hosts)
				{
					host.Start();
					host.ClientAccepted += this.accept;
				}
			}
		}

		/// <summary>
		/// Stops the server and all hosts.
		/// </summary>
		public void Stop()
		{
			lock (this)
			{
				if (!this.running)
					throw new Exception("Server is not running.");

				this.running = false;

				foreach (IHost host in this.hosts)
				{
					host.Stop();
					host.ClientAccepted -= this.accept;
				}
			}
		}

		/// <summary>
		/// Adds a new host to the server.
		/// </summary>
		/// <param name="host"></param>
		public void AddHost(IHost host)
		{
			lock (this)
			{
				if (this.running)
				{
					host.Start();
					host.ClientAccepted += this.accept;
				}

				this.hosts.Add(host);
			}
		}

		/// <summary>
		/// Removes a host from the server.
		/// </summary>
		/// <param name="host"></param>
		/// <returns></returns>
		public bool RemoveHost(IHost host)
		{
			lock (this)
			{
				if (this.hosts.Remove(host))
				{
					if (this.running)
					{
						host.Stop();
						host.ClientAccepted -= this.accept;
					}

					return true;
				}
			}

			return false;
		}

		private void preprocessContext(HttpContext context)
		{
			string connection = context.Request.Header["Connection"];
			if (connection != null && connection.ToLowerInvariant() == "close") 
				context.Response.Header["Connection"] = "close";
			
			context.Response.Header["Server"] = this.ApplicationName;
			context.Response.Header["Date"] = DateTime.Now.ToString("r");

			// find suitable content encoding.
			string[] contentEncodings = context.Request.Header.GetCommaSeparated("Accept-Encoding");
			for (int i = 0; i < contentEncodings.Length; i++)
			{
				int semicolon = contentEncodings[i].IndexOf(';');
				if (semicolon > 0)
					contentEncodings[i] = contentEncodings[i].Substring(0, semicolon);
			}

			// only support gzip for now.
			if (Array.IndexOf(contentEncodings, "gzip") >= 0)
				context.Response.Header["Content-Encoding"] = "gzip";
		}

		private async Task<bool> handleContextAsync(HttpContext context)
		{
			// Copy handlers to avoid locking the collection for the time the request is handled.
			RequestHandler[] localHandlerCopy;
			lock (this.handlerLock)
			{
				localHandlerCopy = this.handlers.ToArray();
			}
			
			// Search for a handler that handles the request.
			foreach (RequestHandler handler in localHandlerCopy)
			{
				if (await handler(context))
				{
					// Request was handled, stop.
					return true;
				}
			}

			// No handler for the request was found.
			return false;
		}

		private void accept(object sender, ConnectionEventArgs e)
		{
			this.acceptAsync((IHost)sender, e).Wait();
		}

		private async Task acceptAsync(IHost host, ConnectionEventArgs e)
		{
			Trace.WriteLine($"{e.EndPoint}: Connected.", "HTTP");
			
			Stream stream = e.Stream;
			
			// Set timeouts in case this is a network stream.
			NetworkStream networkStream = stream as NetworkStream;
			if (networkStream != null)
			{
				networkStream.ReadTimeout = 5000;
				networkStream.WriteTimeout = 5000;
			}

			try
			{
				while (true)
				{
					// Try parsing the request.
					HttpRequest request;
					try
					{
						request = HttpRequest.Parse(stream);
					}
					catch (Exception bug)
					{
						Trace.WriteLine($"{e.EndPoint}: Failed to parse request: {bug}", "HTTP");

						HttpContext errorContext = new HttpContext(this, host, null, stream, e.EndPoint);
						await this.ErrorHandlers[HttpStatus.BadRequest](errorContext);
						return;
					}
					
					// Not sure why some browsers (e.g. chromium) establish a connection but don't send data.
					if (request == null)
						break;

					Trace.WriteLine($"{e.EndPoint}: HTTP {request.Method} {request.URL}", "HTTP");

					// Create a new context based on the request.
					HttpContext context = new HttpContext(this, host, request, stream, e.EndPoint);
					this.preprocessContext(context);

					// Handle request.
					if (!await this.handleContextAsync(context))
					{
						// If no handler was able to serve the request, use the default 404 handler.
						await this.ErrorHandlers[HttpStatus.NotFound](context);
					}

					// Write the response.
					// Can be null in some cases (e.g. web sockets or a request handler that manually wrote a response).
					if (context.Response != null)
					{
						Trace.WriteLine($"{e.EndPoint}: {(int)context.Response.Status} {context.Response.Reason ?? HttpResponse.GetDefaultReason(context.Response.Status)}.", "HTTP");
						await context.Response.WriteAsync(stream);
						stream.Flush();
					}

					// If the response wants to close the connection, exit the loop.
					if (context.Response == null || (context.Response.Header["Connection"] != null && context.Response.Header["Connection"].ToLowerInvariant() == "close"))
						break;
				}
			}
			catch (Exception bug)
			{
				try
				{
					Trace.WriteLine($"{e.EndPoint}: Failed to handle request: {bug}", "HTTP");

					// Send internal error.
					HttpContext context = new HttpContext(this, host, null, stream, e.EndPoint);
					context.UserData = bug;
					await this.ErrorHandlers[HttpStatus.InternalServerError](context);

					context.Response.Header["Connection"] = "close";
					await context.Response.WriteAsync(stream);
				}
				catch (Exception bugception)
				{
					// If that didn't work either, give up.
					Trace.WriteLine($"{e.EndPoint}: Failed to handle error: {bugception}", "HTTP");
				}
			}
			finally
			{
				stream.Close();
				Trace.WriteLine($"{e.EndPoint}: Closed.", "HTTP");
			}
		}

		#endregion
	}

	/// <summary>
	/// Stream extension methods.
	/// </summary>
	internal static class StreamExtensions
	{
		#region Methods

		/// <summary>
		/// Reads a line, unbuffered.
		/// </summary>
		/// <param name="stream"></param>
		/// <returns></returns>
		public static string ReadLine(this Stream stream)
		{
			byte[] buffer = new byte[4096];
			int index = 0;

			int input;

			while ((input = stream.ReadByte()) >= 0)
			{
				buffer[index++] = (byte)input;

				if (index >= 2 && buffer[index - 2] == 13 && buffer[index - 1] == 10)
					return Encoding.UTF8.GetString(buffer, 0, index - 2);
			
				if (index >= 1 && buffer[index - 1] == 10)
					return Encoding.UTF8.GetString(buffer, 0, index - 1);

				if (index == buffer.Length)
					Array.Resize(ref buffer, buffer.Length + 4096);
			}

			if (index == 0)
				throw new EndOfStreamException();

			return Encoding.UTF8.GetString(buffer, 0, index);
		}

		#endregion
	}
}
