﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Http
{
	/// <summary>
	/// Defines the web socket modes.
	/// </summary>
	[Flags]
	public enum WebSocketMode
	{
		Text = 1,
		Binary = 2,
	}

	/// <summary>
	/// Represents a web socket stream.
	/// </summary>
	public sealed class WebSocketStream : Stream
	{
		#region Fields

		
		private readonly WebSocket socket;
		private WebSocketMode writeMode;

		private const int bufferSize = 4096;
		private readonly List<byte[]> buffers;
		private int readPosition;
		private int writePosition;
		private long totalLength;
		
		#endregion

		#region Properties

		public override bool CanRead => true;

		public override bool CanSeek => false;

		public override bool CanWrite => true;

		public override long Length
		{
			get { throw new NotSupportedException(); }
		}

		public override long Position
		{
			get { throw new NotSupportedException(); }
			set { throw new NotSupportedException(); }
		}

		/// <summary>
		/// Gets or sets the read mode of the stream.
		/// The combination Text | Binary is also possible.
		/// </summary>
		public WebSocketMode ReadMode { get; set; }

		/// <summary>
		/// Gets or sets the write mode of the stream.
		/// Either Text or Binary is possible.
		/// </summary>
		public WebSocketMode WriteMode
		{
			get { return this.writeMode; }
			set
			{
				if (value != WebSocketMode.Text && value != WebSocketMode.Binary)
					throw new ArgumentException("Invalid value.");
				this.writeMode = value;
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new WebSocketStream.
		/// </summary>
		public WebSocketStream(WebSocket socket, WebSocketMode readMode, WebSocketMode writeMode)
		{
			this.socket = socket;
			this.socket.FrameRead += this.onFrame;

			if (writeMode != WebSocketMode.Text && writeMode != WebSocketMode.Binary)
				throw new ArgumentException("Invalid value.");

			this.ReadMode = readMode;
			this.writeMode = writeMode;

			this.buffers = new List<byte[]>();
		}

		#endregion

		#region Methods

		private void onFrame(object sender, WebSocketFrameEventArgs e)
		{
			if (((int)e.Frame.OpCode & (int)this.ReadMode) == 0)
				return;

			this.writeToBuffer(e.Frame.Payload, 0, e.Frame.Payload.Length);
		}

		/// <summary>
		/// Reads a byte array from the internal buffer.
		/// </summary>
		/// <param name="buffer"></param>
		/// <param name="offset"></param>
		/// <param name="count"></param>
		/// <returns></returns>
		internal int readFromBuffer(byte[] buffer, int offset, int count)
		{
			int totalBytesRead;

			lock (this.buffers)
			{
				if (count > this.totalLength)
					count = (int)this.totalLength;
				this.totalLength -= count;
				totalBytesRead = count;

				while (count > 0)
				{
					int readBytes = count;
					if (readBytes > bufferSize - this.readPosition)
						readBytes = bufferSize - this.readPosition;

					Array.Copy(this.buffers[0], this.readPosition, buffer, offset, readBytes);
					count -= readBytes;
					offset += readBytes;
					this.readPosition += readBytes;

					if (this.readPosition == bufferSize)
					{
						this.buffers.RemoveAt(0);
						this.readPosition = 0;
					}
				}
			}
			return totalBytesRead;
		}

		/// <summary>
		/// Writes a byte array to the internal buffer.
		/// </summary>
		/// <param name="buffer"></param>
		/// <param name="offset"></param>
		/// <param name="count"></param>
		internal void writeToBuffer(byte[] buffer, int offset, int count)
		{
			lock (this.buffers)
			{
				if (this.buffers.Count == 0)
					this.buffers.Add(new byte[bufferSize]);
				
				this.totalLength += count;
				while (count > 0)
				{
					int writeLength = count;
					if (writeLength > bufferSize - this.writePosition)
						writeLength = bufferSize - this.writePosition;

					Array.Copy(buffer, offset, this.buffers[this.buffers.Count - 1], this.writePosition, writeLength);
					count -= writeLength;
					offset += writeLength;
					this.writePosition += writeLength;
					if (this.writePosition == bufferSize)
					{
						this.buffers.Add(new byte[bufferSize]);
						this.writePosition = 0;
					}
				}
			}
		}
		
		public override int Read(byte[] buffer, int offset, int count)
		{
			while (this.totalLength == 0)
			{
				if (this.socket.ReadFrame() == null)
					return 0;
			}
			
			return this.readFromBuffer(buffer, offset, count);
		}

		public override void Write(byte[] buffer, int offset, int count)
		{
			WebSocketFrame frame = new WebSocketFrame();
			frame.Fin = true;
			if (offset == 0 && count == buffer.Length)
			{
				frame.Payload = buffer;
			}
			else
			{
				frame.Payload = new byte[count];
				Array.Copy(buffer, offset, frame.Payload, 0, count);
			}

			if (this.writeMode == WebSocketMode.Text)
				frame.OpCode = WebSocketOpCode.Text;
			else if (this.writeMode == WebSocketMode.Binary)
				frame.OpCode = WebSocketOpCode.Binary;
			else
				throw new Exception("Invalid write mode.");

			this.socket.WriteFrame(frame, true);
		}
		
		public override long Seek(long offset, SeekOrigin origin)
		{
			throw new NotSupportedException();
		}

		public override void Flush()
		{
			
		}
		
		public override void SetLength(long value)
		{
			throw new NotSupportedException();
		}

		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);
			this.socket.FrameRead -= this.onFrame;
		}

		public override void Close()
		{
			this.socket.Close();
		}

		#endregion

	}
}
