﻿using System.Collections.Generic;

namespace Http
{
	/// <summary>
	/// Contains default handlers for various HTTP status codes.
	/// </summary>
	public sealed class HttpStatusHandlerCollection
	{
		#region Fields

		private readonly Dictionary<HttpStatus, RequestHandler> handlers;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the default handler for the specified HTTP status code (e.g. a page showing an error message).
		/// </summary>
		/// <param name="status"></param>
		/// <returns></returns>
		public RequestHandler this[HttpStatus status]
		{
			get
			{
				RequestHandler handler;
				if (!this.handlers.TryGetValue(status, out handler))
				{
					handler = new DefaultErrorHandler(status).Handle;
					this.handlers[status] = handler;
				}

				return handler;
			}
			set
			{
				this.handlers[status] = value;
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new HttpErrorHandlers.
		/// </summary>
		public HttpStatusHandlerCollection()
		{
			this.handlers = new Dictionary<HttpStatus, RequestHandler>();
		}

		#endregion
	}
}
