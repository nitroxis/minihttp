﻿using System;
using System.IO;

namespace Http
{
	/// <summary>
	/// Defines WebSocket op codes.
	/// </summary>
	public enum WebSocketOpCode
	{
		Continuation = 0x0,
		Text = 0x1,
		Binary = 0x2,
		Close = 0x8,
		Ping = 0x9,
		Pong = 0xA,
	}

	/// <summary>
	/// WebSocketFrame event args.
	/// </summary>
	public sealed class WebSocketFrameEventArgs : EventArgs
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets the web socket frame.
		/// </summary>
		public WebSocketFrame Frame { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new WebSocketFrameEventArgs.
		/// </summary>
		public WebSocketFrameEventArgs(WebSocketFrame frame)
		{
			this.Frame = frame;
		}

		#endregion
	}

	/// <summary>
	/// Represents a web socket frame.
	/// </summary>
	public sealed class WebSocketFrame
	{
		#region Fields

		public bool Fin;
		public bool Rsv1;
		public bool Rsv2;
		public bool Rsv3;
		public WebSocketOpCode OpCode;
		public bool Mask;
		public int MaskingKey;
		public byte[] Payload;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new WebSocketFrame.
		/// </summary>
		public WebSocketFrame()
		{

		}

		#endregion

		#region Methods

		private static byte[] readBytes(Stream stream, int length)
		{
			byte[] buffer = new byte[length];
			int pos = 0;

			while (pos < length)
			{
				int numBytes = stream.Read(buffer, pos, length - pos);
				if (numBytes == 0)
					throw new EndOfStreamException();
				pos += numBytes;
			}

			return buffer;
		}

		public void ApplyMask()
		{
			for (long i = 0; i < this.Payload.LongLength; i++)
				this.Payload[i] = unchecked((byte)(this.Payload[i] ^ (byte)(this.MaskingKey >> (24 - (int)(i % 4) * 8))));

			this.Mask = !this.Mask;
		}

		public void Write(Stream stream)
		{
			BufferedStream bufferedStream = new BufferedStream(stream);
			
			byte b1 = 0;
			
			if (this.Fin) b1 |= 0x80;
			if (this.Rsv1) b1 |= 0x40;
			if (this.Rsv2) b1 |= 0x20;
			if (this.Rsv3) b1 |= 0x10;
			b1 |= (byte)((int)this.OpCode & 0x0F);

			bufferedStream.WriteByte(b1);

			byte b2 = 0;
			if (this.Mask) b2 |= 0x80;

			long payloadLength = this.Payload == null ? 0 : this.Payload.LongLength;

			if (payloadLength < 126)
				b2 |= (byte)payloadLength;
			else if (payloadLength <= ushort.MaxValue)
				b2 |= 126;
			else
				b2 |= 127;

			bufferedStream.WriteByte(b2);

			if (payloadLength >= 126 && payloadLength <= ushort.MaxValue)
			{
				byte[] extPayloadLength = new byte[2];
				extPayloadLength[0] = (byte)(payloadLength >> 8);
				extPayloadLength[1] = (byte)(payloadLength);
				bufferedStream.Write(extPayloadLength, 0, 2);
			}
			else if (payloadLength > ushort.MaxValue)
			{
				byte[] extPayloadLength = new byte[8];
				extPayloadLength[0] = (byte)(payloadLength >> 56);
				extPayloadLength[1] = (byte)(payloadLength >> 48);
				extPayloadLength[2] = (byte)(payloadLength >> 40);
				extPayloadLength[3] = (byte)(payloadLength >> 32);
				extPayloadLength[4] = (byte)(payloadLength >> 24);
				extPayloadLength[5] = (byte)(payloadLength >> 16);
				extPayloadLength[6] = (byte)(payloadLength >> 8);
				extPayloadLength[7] = (byte)(payloadLength);
				bufferedStream.Write(extPayloadLength, 0, 8);
			}

			if (this.Mask)
			{
				byte[] maskingKey = new byte[4];
				maskingKey[0] = (byte)(this.MaskingKey >> 24);
				maskingKey[1] = (byte)(this.MaskingKey >> 16);
				maskingKey[2] = (byte)(this.MaskingKey >> 8);
				maskingKey[3] = (byte)(this.MaskingKey);
				bufferedStream.Write(maskingKey, 0, 4);
			}

			if(this.Payload != null)
				bufferedStream.Write(this.Payload, 0, this.Payload.Length);
			bufferedStream.Flush();
			stream.Flush();
		}

		public static WebSocketFrame Read(Stream stream)
		{
			int b = stream.ReadByte();
			if (b < 0)
				return null;

			WebSocketFrame frame = new WebSocketFrame();
			frame.Fin = (b & 0x80) > 0;
			frame.Rsv1 = (b & 0x40) > 0;
			frame.Rsv2 = (b & 0x20) > 0;
			frame.Rsv3 = (b & 0x10) > 0;
			frame.OpCode = (WebSocketOpCode)(b & 0xF);

			b = stream.ReadByte();
			if (b < 0)
				throw new EndOfStreamException();

			frame.Mask = (b & 0x80) > 0;
			long payloadLength = (b & 0x7F);

			if (payloadLength == 126)
			{
				byte[] extPayloadLength = readBytes(stream, 2);
				payloadLength = (extPayloadLength[0] << 8) | extPayloadLength[1];
			}
			else if (payloadLength == 127)
			{
				byte[] extPayloadLength = readBytes(stream, 8);
				payloadLength = ((long)extPayloadLength[0] << 56) | ((long)extPayloadLength[1] << 48) | ((long)extPayloadLength[2] << 40) | ((long)extPayloadLength[3] << 32) | ((long)extPayloadLength[4] << 24) | ((long)extPayloadLength[5] << 16) | ((long)extPayloadLength[6] << 8) | (long)extPayloadLength[7];
			}

			if (frame.Mask)
			{
				byte[] maskingKey = readBytes(stream, 4);
				frame.MaskingKey = (maskingKey[0] << 24) | (maskingKey[1] << 16) | (maskingKey[2] << 8) | maskingKey[3];
			}

			if (payloadLength > int.MaxValue)
				throw new InvalidDataException("Payload length is too big.");

			frame.Payload = readBytes(stream, (int)payloadLength);
			
			return frame;
		}

		#endregion
	}
}
