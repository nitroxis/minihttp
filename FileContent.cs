﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace Http
{
	/// <summary>
	/// Represents a content provider for files.
	/// </summary>
	public sealed class FileContent : IContent
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets the file info.
		/// </summary>
		public FileInfo File { get; }

		public long Length => this.File.Length;

		public DateTime LastModified => this.File.LastWriteTime;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new FileContent.
		/// </summary>
		public FileContent(string fileName)
		{
			this.File = new FileInfo(fileName);
		}

		/// <summary>
		/// Creates a new FileContent.
		/// </summary>
		public FileContent(FileInfo file)
		{
			this.File = file;
		}

		#endregion

		#region Methods

		public async Task WriteAsync(Stream stream)
		{
			using (Stream input = this.File.OpenRead())
			{
				await input.CopyToAsync(stream);
			}
		}

		#endregion
	}
}
