﻿using System.IO;
using System.Threading.Tasks;

namespace Http
{
	/// <summary>
	/// Represents a HTTP response content provider.
	/// </summary>
	public interface IContent
	{
		/// <summary>
		/// Gets the length of the content (not always needed, e.g. streaming).
		/// </summary>
		long Length { get; }

		/// <summary>
		/// Writes the contents to the specified stream.
		/// </summary>
		/// <param name="stream"></param>
		Task WriteAsync(Stream stream);
	}
}