﻿using System;
using System.Diagnostics;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Http
{
	public delegate void WebSocketCallback(WebSocket socket);

	/// <summary>
	/// Represents a handler class for web socket handshakes.
	/// </summary>
	public class WebSocketHandler
	{
		#region Fields

		private readonly WebSocketCallback callback;
		private readonly string protocol;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new WebSocketHandler.
		/// </summary>
		public WebSocketHandler(WebSocketCallback callback)
			: this(callback, null)
		{

		}

		/// <summary>
		/// Creates a new WebSocketHandler.
		/// </summary>
		public WebSocketHandler(WebSocketCallback callback, string protocol)
		{
			this.callback = callback;
			this.protocol = protocol;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Handles the connection.
		/// </summary>
		/// <param name="context"></param>
		/// <returns></returns>
		public async Task<bool> Handle(HttpContext context)
		{
			if (context.Request.Method != HttpMethod.Get)
				return false;

			// check for Connection: Upgrade.
			string[] connection = context.Request.Header.GetCommaSeparated("Connection");
			
			bool hasUpgrade = false;
			foreach (string connectionStr in connection)
			{
				if (connectionStr.ToLowerInvariant() == "upgrade")
					hasUpgrade = true;
			}

			if (!hasUpgrade)
				return false;

			// check for Upgrade: websockets.
			string upgrade = context.Request.Header["Upgrade"];
			if (upgrade == null)
				return false;

			if (upgrade.ToLowerInvariant() != "websocket")
				return false;


			// check version.
			if (context.Request.Header["Sec-WebSocket-Version"] != "13")
				return false;

			// get nonce.
			string nonceStr = context.Request.Header["Sec-WebSocket-Key"];
			if (nonceStr == null)
				return false;

			//byte[] nonce = Convert.FromBase64String(nonceStr);


			// get sub protocols.
			string protocols = context.Request.Header["Sec-WebSocket-Protocol"];
			string[] protocolsSplit;

			if (string.IsNullOrWhiteSpace(protocols))
			{
				protocolsSplit = new string[0];
			}
			else
			{
				protocolsSplit = protocols.Split(new[] {','});
				for (int i = 0; i < protocolsSplit.Length; i++)
					protocolsSplit[i] = protocolsSplit[i].Trim();
			}

			// check sub protocol.
			string subProtocol;

			if (this.protocol == null)
			{
				if (protocolsSplit.Length == 0)
					subProtocol = null;
				else
					subProtocol = protocolsSplit[0];
			}
			else
			{
				bool match = false;
				subProtocol = this.protocol.ToLowerInvariant();
				foreach (string proto in protocolsSplit)
				{
					if (proto.ToLowerInvariant() == subProtocol)
					{
						match = true;
						break;
					}
				}

				if (!match)
				{
					Trace.WriteLine($"Invalid WebSocket sub protocol \"{protocols}\", expected \"{this.protocol}\".");
					context.Response.Status = HttpStatus.BadRequest;
					return false;
				}
			}

			// configure response.
			context.Response.Status = HttpStatus.SwitchingProtocols;
			context.Response.Reason = "Web Socket Protocol Handshake";
			context.Response.Header["Upgrade"] = "websocket";
			context.Response.Header["Connection"] = "Upgrade";
			context.Response.Header["Sec-WebSocket-Accept"] = Convert.ToBase64String(SHA1.Create().ComputeHash(Encoding.ASCII.GetBytes(nonceStr + "258EAFA5-E914-47DA-95CA-C5AB0DC85B11")));
			if (subProtocol != null)
				context.Response.Header["Sec-WebSocket-Protocol"] = subProtocol;

			await context.Response.WriteAsync(context.Stream);
			Trace.WriteLine($"WebSocket opened (URL: \"{context.Request.URL}\").", "HTTP");

			WebSocket socket = new WebSocket(context.Stream, subProtocol);
			this.callback(socket);

			context.Response = null; // don't write the response again!
			return true;
		}

		#endregion
	}
}
