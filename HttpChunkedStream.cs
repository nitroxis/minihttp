﻿using System;
using System.IO;
using System.Text;

namespace Http
{
	/// <summary>
	/// Represents a HTTP chunked transfer encoding stream.
	/// </summary>
	public sealed class HttpChunkedStream : Stream
	{
		#region Fields

		private readonly Stream stream;
		private readonly bool leaveOpen;

		private byte[] readBuffer;
		private int readPosition;

		#endregion

		#region Properties
		
		public override bool CanRead => this.stream.CanRead;

		public override bool CanSeek => false;

		public override bool CanWrite => this.stream.CanWrite;

		public override long Length => throw new NotSupportedException();

		public override long Position
		{
			get => throw new NotSupportedException();
			set => throw new NotSupportedException();
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new <see cref="HttpChunkedStream"/>.
		/// </summary>
		public HttpChunkedStream(Stream stream)
			: this(stream, false)
		{
			
		}

		/// <summary>
		/// Creates a new <see cref="HttpChunkedStream"/>.
		/// </summary>
		public HttpChunkedStream(Stream stream, bool leaveOpen)
		{
			this.stream = stream;
			this.leaveOpen = leaveOpen;
		}

		#endregion

		#region Methods

		private void writeEndChunk()
		{
			this.stream.Write(new byte[] {48, 13, 10, 13, 10}, 0, 5); // 0\r\n\r\n
		}

		private bool readChunk()
		{
			this.readPosition = 0;

			StringBuilder header = new StringBuilder();
			while (true)
			{
				int input = this.stream.ReadByte();
				if (input < 0)
				{
					if (header.Length > 0)
						throw new EndOfStreamException();
					return false;
				}

				header.Append((char)input);
				if (header.Length >= 2 && header[header.Length - 2] == '\r' && header[header.Length - 1] == '\n')
				{
					string lengthStr = header.ToString().Trim();
					int length = Convert.ToInt32(lengthStr, 16);
					int currentPosition = 0;
					this.readBuffer = new byte[length];

					while (currentPosition < length)
					{
						int numBytes = this.stream.Read(this.readBuffer, currentPosition, length - currentPosition);
						if (numBytes == 0)
							throw new EndOfStreamException();

						currentPosition += numBytes;
					}

					// read \r\n trail.
					int trail = this.stream.ReadByte();
					if(trail == -1)
						throw new EndOfStreamException();
					if (trail != 13)
						throw new InvalidDataException();

					trail = this.stream.ReadByte();
					if (trail == -1)
						throw new EndOfStreamException();
					if (trail != 10)
						throw new InvalidDataException();

					return length != 0;
				}

				if (header.Length > 256)
					throw new InvalidDataException("Chunked tranfer encoding header is too big.");
			}
		}

		public override int Read(byte[] buffer, int offset, int count)
		{
			if (this.readBuffer == null || this.readPosition == this.readBuffer.Length)
			{
				if (!this.readChunk())
					return 0;
			}

			if (count > this.readBuffer.Length - this.readPosition)
				count = this.readBuffer.Length - this.readPosition;

			Array.Copy(this.readBuffer, this.readPosition, buffer, offset, count);

			this.readPosition += count;
			return count;
		}

		public override void Write(byte[] buffer, int offset, int count)
		{
			string header = count.ToString("X");
			int headerLen = Encoding.ASCII.GetByteCount(header);

			byte[] encoded = new byte[count + headerLen + 4];
			Encoding.ASCII.GetBytes(header, 0, header.Length, encoded, 0);

			int pos = headerLen;
			encoded[pos++] = 13;
			encoded[pos++] = 10;

			Array.Copy(buffer, offset, encoded, pos, count);
			pos += count;

			encoded[pos++] = 13;
			encoded[pos++] = 10;
			
			this.stream.Write(encoded, 0, encoded.Length);

			/*
			while (count > 0)
			{
				int numBytes = count;
				if (numBytes > this.writeBuffer.Length - this.writeBufferLength)
					numBytes = this.writeBuffer.Length - this.writeBufferLength;

				Array.Copy(buffer, offset, this.writeBuffer, this.writeBufferLength, numBytes);
				this.writeBufferLength += numBytes;

				if (this.writeBufferLength == this.writeBuffer.Length)
					this.writeChunk();

				count -= numBytes;
				offset += numBytes;
			}*/
		}
		
		public override void Flush()
		{
			this.stream.Flush();
		}

		public override void Close()
		{
			this.Flush();
			this.writeEndChunk();
			this.stream.Flush();

			if (!this.leaveOpen)
				this.stream.Close();
		}
		
		public override long Seek(long offset, SeekOrigin origin)
		{
			throw new NotSupportedException();
		}

		public override void SetLength(long value)
		{
			throw new NotSupportedException();
		}

		#endregion
		
	}
}
