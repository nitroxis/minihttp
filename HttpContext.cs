﻿using System.IO;
using System.Net;

namespace Http
{
	/// <summary>
	/// Represents an HTTP request handler context.
	/// </summary>
	public class HttpContext
	{
		#region Properties

		/// <summary>
		/// Gets or sets the server that hosts this session.
		/// </summary>
		public HttpServer Server { get; }

		/// <summary>
		/// Gets the host.
		/// </summary>
		public IHost Host { get; }

		/// <summary>
		/// Gets the HTTP request.
		/// </summary>
		public HttpRequest Request { get; }

		/// <summary>
		/// Gets or sets the response to the request.
		/// Can be set to null to not send a response.
		/// </summary>
		public HttpResponse Response { get; set; }

		/// <summary>
		/// Gets the stream.
		/// This can be used to read the request body or write the response.
		/// </summary>
		public Stream Stream { get; }

		/// <summary>
		/// Gets the remote end point.
		/// </summary>
		public EndPoint EndPoint { get; }

		/// <summary>
		/// Gets or sets a user-defined object.
		/// </summary>
		public object UserData { get; set; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new HttpContext.
		/// </summary>
		public HttpContext(HttpServer server, IHost host, HttpRequest request, Stream stream, EndPoint endPoint)
		{
			this.Server = server;
			this.Host = host;
			this.Request = request;
			this.Response = new HttpResponse();
			this.Stream = stream;
			this.EndPoint = endPoint;
			this.UserData = null;
		}

		#endregion
	}
}
