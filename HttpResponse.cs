﻿using System;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Http
{
	/// <summary>
	/// Represents an HTTP response.
	/// </summary>
	public class HttpResponse
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the response status code.
		/// </summary>
		public HttpStatus Status { get; set; }

		/// <summary>
		/// Gets or sets the reason for the status. Set to null for default reason for status code.
		/// </summary>
		public string Reason { get; set; }

		/// <summary>
		/// Gets or sets the response header.
		/// </summary>
		public HttpHeader Header { get; set; }

		/// <summary>
		/// Gets or sets the content of the HTTP response.
		/// </summary>
		public IContent Content { get; set; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new HttpResponse.
		/// </summary>
		public HttpResponse()
		{
			this.Status = HttpStatus.OK;
			this.Reason = null;
			this.Header = new HttpHeader();
		}

		#endregion

		#region Methods

		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append("HTTP/1.1 ");
			builder.Append((int)this.Status);
			builder.Append(" ");

			if (this.Reason == null)
				builder.Append(HttpResponse.GetDefaultReason(this.Status));
			else
				builder.Append(this.Reason);

			builder.AppendLine();
			builder.Append(this.Header);
			builder.AppendLine();

			return builder.ToString();
		}
		
		public Task WriteAsync(Stream stream)
		{
			return this.WriteAsync(stream, false);
		}

		public async Task WriteAsync(Stream stream, bool headerOnly)
		{
			if (headerOnly || this.Content == null)
			{
				await this.writeHeaderAsync(stream);
				return;
			}
	
			if (this.Content != null)
			{
				bool useChunkedTransfer = false;
				if (this.Header["Content-Length"] == null)
				{
					if (this.Header["Connection"] == null || this.Header["Connection"].ToLowerInvariant() != "close")
					{
						useChunkedTransfer = true;
						this.Header["Transfer-Encoding"] = "chunked";
					}
				}

				string encoding = this.Header["Content-Encoding"];
				if (encoding == null)
					encoding = "identity";
				encoding = encoding.ToLowerInvariant();

				if (encoding == "identity")
				{
					await this.writeHeaderAsync(stream);

					if (useChunkedTransfer)
					{
						using (HttpChunkedStream chunkedStream = new HttpChunkedStream(stream))
						{
							await this.Content.WriteAsync(chunkedStream);	
						}
					}
					else
					{
						await this.Content.WriteAsync(stream);	
					}
				}
				else if (encoding == "gzip")
				{
					this.Header["Content-Length"] = null;
					this.Header["Transfer-Encoding"] = "chunked";

					await this.writeHeaderAsync(stream);

					using (HttpChunkedStream chunkedStream = new HttpChunkedStream(stream))
					using (GZipStream gzipStream = new GZipStream(chunkedStream, CompressionLevel.Optimal, true))
					{
						await this.Content.WriteAsync(gzipStream);
					}
				}
				else
				{
					throw new NotSupportedException($"Content-Encoding {encoding} is not supported.");
				}
			}
		}

		private async Task writeHeaderAsync(Stream stream)
		{
			byte[] headerData = Encoding.ASCII.GetBytes(this.ToString());
			await stream.WriteAsync(headerData, 0, headerData.Length);
		}

		/// <summary>
		/// Returns the default reason string for the specified <see cref="HttpStatus"/>.
		/// </summary>
		/// <param name="status"></param>
		/// <returns></returns>
		public static string GetDefaultReason(HttpStatus status)
		{
			switch (status)
			{
				case HttpStatus.SwitchingProtocols:
					return "Switching Protocols";

				case HttpStatus.BadRequest:
					return "Bad Request";
				case HttpStatus.NotFound:
					return "Not Found";

				case HttpStatus.InternalServerError:
					return "Internal Server Error";
				case HttpStatus.NotImplemented:
					return "Not Implemented";
				case HttpStatus.BadGateway:
					return "Bad Gateway";
				case HttpStatus.ServiceUnavailable:
					return "Service Unavailable";
				case HttpStatus.GatewayTimeout:
					return "Gateway Timeout";
				case HttpStatus.HTTPVersionNotSupported:
					return "HTTP Version Not Supported";

				default:
					return status.ToString();
			}
		}

		/// <summary>
		/// Sets the response content based on the specified string.
		/// The content type will be set the text/plain and UTF-8 will be used to encode the string.
		/// </summary>
		/// <param name="str"></param>
		public void SetContentString(string str)
		{
			this.Content = new StringContent(str, Encoding.UTF8);
			this.Header["Content-Length"] = this.Content.Length.ToString(CultureInfo.InvariantCulture);
			this.Header["Content-Type"] = "text/plain; charset=utf-8";
		}
		
		/// <summary>
		/// Sets the response content based on the specified string and content type.
		/// </summary>
		/// <param name="str"></param>
		/// <param name="contentType"></param>
		public void SetContentString(string str, string contentType)
		{
			this.Content = new StringContent(str, Encoding.UTF8);
			this.Header["Content-Length"] = this.Content.Length.ToString(CultureInfo.InvariantCulture);
			this.Header["Content-Type"] = contentType;
		}

		/// <summary>
		/// Sets the response content using a file.
		/// </summary>
		/// <param name="fileName"></param>
		/// <param name="contentType"></param>
		public void SetContentFile(string fileName, string contentType)
		{
			FileContent fileContent = new FileContent(fileName);
			this.Content = fileContent;
			this.Header["Content-Length"] = fileContent.Length.ToString(CultureInfo.InvariantCulture);
			this.Header["Content-Type"] = contentType;
			this.Header["Last-Modified"] = fileContent.LastModified.ToString("r");
		}

		#endregion
	}
}
