﻿namespace Http
{
	/// <summary>
	/// Defines the different HTTP request methods.
	/// </summary>
	public enum HttpMethod
	{
		/// <summary>
		/// HTTP GET method.
		/// </summary>
		Get,

		/// <summary>
		/// HTTP HEAD method.
		/// </summary>
		Head,

		/// <summary>
		/// HTTP POST method.
		/// </summary>
		Post
	}
}
