﻿using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Http
{
	/// <summary>
	/// Represents simple string content.
	/// </summary>
	public sealed class StringContent : IContent
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the content.
		/// </summary>
		public string Content { get; set; }

		/// <summary>
		/// Gets or sets the content encoding.
		/// </summary>
		public Encoding Encoding { get; set; }

		/// <summary>
		/// Gets the length of the content.
		/// </summary>
		public long Length => this.Encoding.GetByteCount(this.Content);

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new StringContent.
		/// </summary>
		public StringContent()
			: this("")
		{

		}

		/// <summary>
		/// Creates a new StringContent.
		/// </summary>
		public StringContent(string content)
			: this(content, Encoding.UTF8)
		{

		}

		/// <summary>
		/// Creates a new StringContent.
		/// </summary>
		public StringContent(string content, Encoding encoding)
		{
			this.Content = content;
			this.Encoding = encoding;
		}

		#endregion

		#region Methods

		public Task WriteAsync(Stream stream)
		{
			byte[] bytes = this.Encoding.GetBytes(this.Content);
			stream.Write(bytes, 0, bytes.Length);
			return Task.CompletedTask;
		}

		#endregion
	}
}