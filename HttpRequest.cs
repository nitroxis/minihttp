﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Http
{
	/// <summary>
	/// Represents a HTTP request.
	/// </summary>
	public class HttpRequest
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the method of the request.
		/// </summary>
		public HttpMethod Method { get; set; }

		/// <summary>
		/// Gets or sets the request header.
		/// </summary>
		public HttpHeader Header { get; set; }

		/// <summary>
		/// Gets or sets the requested URL.
		/// </summary>
		public string URL { get; set; }

		/// <summary>
		/// Gets or sets the query fields.
		/// </summary>
		public Dictionary<string, string> Query { get; set; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new HttpRequest.
		/// </summary>
		public HttpRequest()
		{
			this.Query = new Dictionary<string, string>();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Converts the request to a string.
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			
			switch (this.Method)
			{
				case HttpMethod.Get:
					builder.Append("GET");
					break;

				case HttpMethod.Head:
					builder.Append("HEAD");
					break;

				case HttpMethod.Post:
					builder.Append("POST");
					break;
			}

			builder.Append(" ");
			builder.Append(this.URL);

			if (this.Query.Count > 0)
			{
				builder.Append("?");

				bool first = true;
				foreach (KeyValuePair<string, string> field in this.Query)
				{
					if (first)
						first = false;
					else
						builder.Append("&");
					builder.Append(field.Key);
					if (field.Value != null)
					{
						builder.Append("=");
						builder.Append(field.Value);
					}
				}
			}

			builder.AppendLine(" HTTP/1.1");
			builder.Append(this.Header);
			builder.AppendLine();

			return builder.ToString();
		}

		/// <summary>
		/// Parses a HTTP request from a text reader.
		/// </summary>
		/// <param name="stream">The stream to read from.</param>
		/// <returns></returns>
		public static HttpRequest Parse(Stream stream)
		{
			string requestLine = stream.ReadLine();
			if (string.IsNullOrEmpty(requestLine))
				return null;

			string[] split = requestLine.Split(' ');
			if (split.Length != 3)
				throw new FormatException("Invalid HTTP request.");

			HttpRequest request = new HttpRequest();

			switch (split[0].ToUpperInvariant())
			{
				case "GET":
					request.Method = HttpMethod.Get;
					break;

				case "HEAD":
					request.Method = HttpMethod.Head;
					break;

				case "POST":
					request.Method = HttpMethod.Post;
					break;

				default:
					throw new FormatException($"Invalid HTTP request method \"{split[0]}\".");
			}

			request.Header = HttpHeader.Parse(stream);

			int queryStart = split[1].IndexOf('?');
			if (queryStart >= 0)
			{
				request.URL = Uri.UnescapeDataString(split[1].Substring(0, queryStart));

				string[] queryFields = split[1].Substring(queryStart + 1).Split('&');
				foreach (string field in queryFields)
				{
					int equalsPosition = field.IndexOf('=');
					string name;
					string value = null;
					if (equalsPosition > 0)
					{
						name = field.Substring(0, equalsPosition);
						value = Uri.UnescapeDataString(field.Substring(equalsPosition + 1));
					}
					else
					{
						name = field;
					}
					request.Query.Add(name, value);
				}
			}
			else
			{
				request.URL = Uri.UnescapeDataString(split[1]);
			}
			
			return request;
		}

		#endregion
	}
}
