﻿using System;
using System.IO;
using System.Text;
using System.Threading;

namespace Http
{
	/// <summary>
	/// Represents a web socket.
	/// </summary>
	public sealed class WebSocket
	{
		#region Events

		public event EventHandler<WebSocketFrameEventArgs> FrameRead;

		#endregion

		#region Fields

		private readonly Stream stream;
		private readonly Random maskingKeyGenerator;
		private bool closed;

		private readonly object readLock;
		private readonly object writeLock;

		// ReadFrame() synchronization.
		private readonly object readMutex;
		private bool isReading;
		private readonly ManualResetEvent readWait;
		private readonly ManualResetEvent readWaitFinished;
		private int numWaitingThreads;
		private volatile WebSocketFrame readResult;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the encoding used for text messages.
		/// </summary>
		public Encoding Encoding { get; set; }

		/// <summary>
		/// Gets the sub protocol.
		/// </summary>
		public string Protocol { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new WebSocket.
		/// </summary>
		public WebSocket(Stream stream, string protocol)
		{
			this.stream = stream;
			this.Protocol = protocol;
			this.Encoding = Encoding.UTF8;
			this.maskingKeyGenerator = new Random();
			this.closed = false;

			this.readLock = new object();
			this.writeLock = new object();

			this.readMutex = new object();
			this.isReading = false;
			this.readWait = new ManualResetEvent(false);
			this.readWaitFinished = new ManualResetEvent(false);
			this.numWaitingThreads = 0;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Reads one web socket frame.
		/// </summary>
		/// <returns></returns>
		public WebSocketFrame ReadFrame()
		{
			if (this.closed)
				return null;

			bool doWork = false;

			// determine if this any other thread is currently doing the work.
			lock (this.readMutex)
			{
				if (this.isReading)
				{
					// this thread will wait, increase number of waiting threads.
					this.numWaitingThreads++;
				}
				else
				{
					// this thread has to do the work.
					this.readWait.Reset(); // make sure all other threads wait.
					this.isReading = true; // this thread is working on it.
					doWork = true;
				}
			}

			if (doWork)
			{
				// do the work and store the result.
				this.readResult = this.readFrame();

				// synchronize access to state.
				lock (this.readMutex)
				{
					// is there any waiting thread?
					if (this.numWaitingThreads > 0)
					{
						this.readWaitFinished.Reset(); // make sure the wait handle is not signaled.
						this.readWait.Set(); // let all currently waiting threads finish.
						this.readWaitFinished.WaitOne(); // wait for them.
					}

					this.isReading = false; // this thread no longer working.

					return this.readResult;
				}
			}

			// wait for the currently working thread to provide the result.
			this.readWait.WaitOne();

			// fetch the result here.
			WebSocketFrame result = this.readResult;

			// if this is the last waiting thread, signal the wait handle for the working thread.
			if (Interlocked.Decrement(ref this.numWaitingThreads) == 0)
				this.readWaitFinished.Set();

			return result;
		}

		private WebSocketFrame readFrame()
		{
			lock (this.readLock)
			{
				WebSocketFrame frame = WebSocketFrame.Read(this.stream);

				if (frame.Mask)
					frame.ApplyMask();

				if (frame.OpCode == WebSocketOpCode.Ping)
				{
					WebSocketFrame pong = new WebSocketFrame();
					pong.Fin = true;
					pong.OpCode = WebSocketOpCode.Pong;
					this.WriteFrame(pong);
				}
				else if (frame.OpCode == WebSocketOpCode.Close)
				{
					this.closed = true;
				}

				if (this.FrameRead != null)
					this.FrameRead(this, new WebSocketFrameEventArgs(frame));

				return frame;
			}
		}

		/// <summary>
		/// Writes a web socket frame.
		/// </summary>
		/// <param name="frame">The frame to write.</param>
		public void WriteFrame(WebSocketFrame frame)
		{
			if (this.closed)
				throw new Exception("WebSocket is closed.");

			lock(this.writeLock)
				frame.Write(this.stream);
		}

		/// <summary>
		/// Writes a web socket frame.
		/// </summary>
		/// <param name="frame">The frame to write.</param>
		/// <param name="generateMask">Specifies whether the method should generate a new mask.</param>
		public void WriteFrame(WebSocketFrame frame, bool generateMask)
		{
			if (this.closed)
				throw new Exception("WebSocket is closed.");

			if (generateMask)
			{
				if (frame.Mask)
					frame.ApplyMask();

				frame.MaskingKey = this.maskingKeyGenerator.Next(int.MinValue, int.MaxValue);
				frame.ApplyMask();
			}

			lock (this.writeLock)
				frame.Write(this.stream);
		}
		
		/// <summary>
		/// Sends the specfied text.
		/// </summary>
		/// <param name="text"></param>
		public void Send(string text)
		{
			WebSocketFrame frame = new WebSocketFrame();
			frame.Fin = true;
			frame.OpCode = WebSocketOpCode.Text;
			frame.Payload = this.Encoding.GetBytes(text);
			frame.MaskingKey = this.maskingKeyGenerator.Next(int.MinValue, int.MaxValue);
			frame.Mask = false;
			frame.ApplyMask();
			this.WriteFrame(frame);
		}

		/// <summary>
		/// Closese the WebSocket and sends a close packet.
		/// </summary>
		public void Close()
		{
			lock (this)
			{
				if (this.closed)
					return;

				WebSocketFrame frame = new WebSocketFrame();
				frame.Fin = true;
				frame.OpCode = WebSocketOpCode.Close;
				try
				{
					this.WriteFrame(frame);
				}
				finally
				{
					this.closed = true;
				}
			}
		}

		#endregion
	}
}
