﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;

namespace Http
{
	/// <summary>
	/// Stream event args.
	/// </summary>
	public sealed class ConnectionEventArgs : EventArgs
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets the stream.
		/// </summary>
		public Stream Stream { get; }

		/// <summary>
		/// Gets the end point of the client.
		/// </summary>
		public EndPoint EndPoint { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new StreamEventArgs.
		/// </summary>
		public ConnectionEventArgs(Stream stream, EndPoint endPoint)
		{
			this.Stream = stream;
			this.EndPoint = endPoint;
		}

		#endregion
	}

	/// <summary>
	/// Represents an interface for hosts, capable of accepted clients.
	/// </summary>
	public interface IHost
	{
		event EventHandler<ConnectionEventArgs> ClientAccepted;

		string Name { get; }
		int Port { get; }

		void Start();
		void Stop();
	}

	/// <summary>
	/// Represents a standard HTTP host.
	/// </summary>
	public class HttpHost : IHost
	{
		#region Event

		public event EventHandler<ConnectionEventArgs> ClientAccepted;
		
		#endregion

		#region Fields

		private readonly TcpListener listener;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the host's name.
		/// </summary>
		public string Name { get; }

		/// <summary>
		/// Gets the IP address of the local end point.
		/// </summary>
		public IPAddress Address { get; }

		/// <summary>
		/// Gets the port the server is running on.
		/// </summary>
		public int Port { get; }

		#endregion

		#region Constructors

		public HttpHost(IPAddress localAddress, int port)
			: this(localAddress, port, localAddress.ToString())
		{

		}
		
		public HttpHost(IPAddress localAddress, int port, string hostName)
		{
			this.listener = new TcpListener(localAddress, port);
			this.Address = localAddress;
			this.Port = port;
			this.Name = hostName;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Starts listening for incoming connections.
		/// </summary>
		public void Start()
		{
			Trace.WriteLine(string.Format("{1}:{2}: Started {0}.", this.GetType().Name, this.Address, this.Port), "HTTP");
			
			this.listener.Start();
			this.beginAccept();
		}

		/// <summary>
		/// Stops the listener.
		/// </summary>
		public void Stop()
		{
			Trace.WriteLine(string.Format("{1}:{2}: Stopped {0}.", this.GetType().Name, this.Address, this.Port), "HTTP");

			this.listener.Stop();
		}

		private void beginAccept()
		{
			try
			{
				this.listener.BeginAcceptTcpClient(this.endAccept, null);
			}
			catch (Exception ex)
			{
				Trace.WriteLine($"Could not begin accepting TCP clients: {ex.Message}", "HTTP");
			}
		}

		private void endAccept(IAsyncResult result)
		{
			TcpClient client = null;
			try
			{
				client = this.listener.EndAcceptTcpClient(result);
			}
			catch (Exception ex)
			{
				Trace.WriteLine($"Could not accept TCP client: {ex.Message}", "HTTP");
			}

			this.beginAccept();

			if(client != null)
				this.AcceptClient(client);
		}

		protected virtual void AcceptClient(TcpClient client)
		{
			this.OnClientAccepted(new ConnectionEventArgs(client.GetStream(), client.Client.RemoteEndPoint));
		}

		protected virtual void OnClientAccepted(ConnectionEventArgs e)
		{
			this.ClientAccepted?.Invoke(this, e);
		}

		#endregion
	}

	/// <summary>
	/// Represents a secure HTTP host (HTTPS).
	/// </summary>
	public class HttpsHost : HttpHost
	{
		#region Fields

		#endregion

		#region Properties

		public X509Certificate Certificate { get; }

		#endregion

		#region Constructors

		public HttpsHost(IPAddress localAddress, int port, X509Certificate certificate)
			: base(localAddress, port)
		{
			this.Certificate = certificate;
		}

		public HttpsHost(IPAddress localAddress, int port, X509Certificate certificate, string hostName)
			: base(localAddress, port, hostName)
		{
			this.Certificate = certificate;
		}

		#endregion

		#region Methods

		protected override void AcceptClient(TcpClient client)
		{
			SslStream stream = new SslStream(client.GetStream());

			try
			{
				Trace.WriteLine($"{client.Client.RemoteEndPoint}: SSL authentification.", "HTTP");
				stream.AuthenticateAsServer(this.Certificate, false, SslProtocols.Tls, true);
			}
			catch (IOException bug) // ssl handshake failed.
			{
				Trace.WriteLine($"{client.Client.RemoteEndPoint}: SSL failed: {bug.Message}", "HTTP");
				stream.Close();
				return;
			}

			this.OnClientAccepted(new ConnectionEventArgs(stream, client.Client.RemoteEndPoint));
		}

		#endregion

	}
}
