﻿using System;
using System.Globalization;
using System.Threading.Tasks;

namespace Http
{
	/// <summary>
	/// Represents the standard error handler.
	/// </summary>
	public sealed class DefaultErrorHandler
	{
		#region Fields

		private readonly HttpStatus status;

		#endregion
		
		#region Constructors

		/// <summary>
		/// Creates a new <see cref="DefaultErrorHandler"/>.
		/// </summary>
		public DefaultErrorHandler(HttpStatus status)
		{
			this.status = status;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Handles the request. The response will be a generic status/error page.
		/// </summary>
		/// <param name="context"></param>
		/// <returns></returns>
		public Task<bool> Handle(HttpContext context)
		{
			string reason = HttpResponse.GetDefaultReason(this.status);
			string description = "No details available.";

			switch (this.status)
			{
				case HttpStatus.Forbidden:
				{
					description = $"You don't have to permission to access {context.Request.URL} on this server.";
					break;
				}
				case HttpStatus.NotFound:
				{
					description = $"The requested URL {context.Request.URL} was not found on this server.";
					break;
				}
				case HttpStatus.InternalServerError:
				{
					Exception bug = context.UserData as Exception;
					if (bug != null)
						description = "An error occured while handling your request: " + bug;
					else
						description = "An error occured while handling your request.";
					break;
				}
			}

			description = description.Replace(Environment.NewLine, "<br/>");

			string statusStr = ((int)this.status).ToString(CultureInfo.InvariantCulture);

			string page =
				"<!DOCTYPE html>" +
				"<html>" +
					"<head>" +
						"<title>" + statusStr + " " + reason + "</title>" +
						"<style>" +
							"html, body { background-color:#eee; font-family:sans-serif; text-align:center; padding:0; margin:0; }" +
							"h1 { font-size:6em; color:#eee; margin:0; }" +
							"h2 { font-size:3em; color:#eee; margin:0; }" +
							"p { color:#eee; }" +
							"div { background-color:#e43; padding-top:6em; padding-bottom:6em; margin-bottom:2em; }" +
							"address { color:#666; font-style:normal; }" +
						"</style>" +
					"</head>" +
					"<body>" +
						"<div><h1>" + statusStr + "</h1><h2>" + reason + "</h2><p>" + description + "</p></div>" +
						"<address>" + context.Server.ApplicationName + " at " + context.Host.Name + " Port " + context.Host.Port.ToString(CultureInfo.InvariantCulture) + "</address>" +
					"</body>" +
				"</html>";
			
			context.Response.Content = new StringContent(page);
			context.Response.Header["Content-Type"] = "text/html";
			context.Response.Status = this.status;

			return Task.FromResult(true);
		}

		#endregion
	}
}
