﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Http
{
	/// <summary>
	/// Represents a HTTP request or response header.
	/// </summary>
	public sealed class HttpHeader : IEnumerable<HttpHeaderField>
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the list of fields in the header.
		/// </summary>
		public List<HttpHeaderField> Fields { get; set; }

		/// <summary>
		/// Gets or sets the field with the specified name.
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		public string this[string name]
		{
			get
			{
				name = name.ToLowerInvariant();

				foreach (HttpHeaderField field in this.Fields)
				{
					if (field.Name.ToLowerInvariant() == name)
						return field.Value;
				}

				return null;
			}
			set
			{
				string nameLower = name.ToLowerInvariant();
				
				foreach (HttpHeaderField field in this.Fields)
				{
					if (field.Name.ToLowerInvariant() == nameLower)
					{
						if (value == null)
						{
							this.Fields.Remove(field);
						}
						else
						{
							field.Value = value;
							field.Name = name;
						}
						return;
					}
				}

				if (value == null)
					return;

				HttpHeaderField newField = new HttpHeaderField(name, value);
				this.Fields.Add(newField);
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new HttpHeader.
		/// </summary>
		public HttpHeader()
		{
			this.Fields = new List<HttpHeaderField>();
		}

		#endregion

		#region Methods

		public IEnumerator<HttpHeaderField> GetEnumerator()
		{
			return this.Fields.GetEnumerator();
		}

		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			return this.Fields.GetEnumerator();
		}
		
		/// <summary>
		/// Returns the values of a comma-separated header field.
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		public string[] GetCommaSeparated(string name)
		{
			string value = this[name];
			if (string.IsNullOrEmpty(value))
				return new string[0];

			string[] split = value.Split(new[] {','});
			for (int i = 0; i < split.Length; i++)
				split[i] = split[i].Trim();

			return split;
		}
		
		/// <summary>
		/// Converts the HTTP header to a string, ready to use for the HTTP protocol.
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();

			foreach (HttpHeaderField field in this.Fields)
				builder.AppendLine(field.ToString());

			return builder.ToString();
		}

		/// <summary>
		/// Parses a HTTP 
		/// </summary>
		/// <param name="stream">The stream to read from.</param>
		/// <returns></returns>
		public static HttpHeader Parse(Stream stream)
		{
			string line;
			HttpHeader header = new HttpHeader();
			while (!string.IsNullOrEmpty(line = stream.ReadLine()))
			{
				string[] keyValue = line.Split(new char[] {':'}, 2);
				if (keyValue.Length != 2)
					throw new FormatException("Invalid HTTP header.");
				HttpHeaderField field = new HttpHeaderField(keyValue[0].Trim(), keyValue[1].Trim());
				header.Fields.Add(field);
			}
			return header;
		}

		#endregion
	}

	/// <summary>
	/// Represents a HTTP header field.
	/// </summary>
	public sealed class HttpHeaderField
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the name of the field
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets the value of the field.
		/// </summary>
		public string Value { get; set; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new HttpHeaderField.
		/// </summary>
		public HttpHeaderField(string name, string value)
		{
			this.Name = name;
			this.Value = value;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Converts the field to a string.
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			return $"{this.Name}: {this.Value}";
		}

		#endregion
	}
}
